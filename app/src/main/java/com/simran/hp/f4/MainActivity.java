package com.simran.hp.f4;

import android.app.Dialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button start, stop;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        start = (Button) findViewById(R.id.f6);

        start.setOnClickListener(this);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                Intent intent = new Intent(MainActivity.this, navbottom.class);
                startActivity(intent);
            }


        }, 5000);


    }

    @Override
    public void onClick(View view) {
        if (view == start)
        {
        startService(new Intent(this,MyService.class));
        }
         else if(view == stop);
        {
            stopService(new Intent(this,MyService.class));
        }
    }
}